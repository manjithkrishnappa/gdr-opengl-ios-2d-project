//
//  AppDelegate.h
//  GDR_OGL2D
//
//  Created by Manjith on 11/10/13.
//  Copyright (c) 2013 PGS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
