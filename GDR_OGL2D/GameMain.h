//
//  GameMain.h
//  GDR_OGL2D
//
//  Created by Manjith on 11/10/13.
//  Copyright (c) 2013 PGS. All rights reserved.
//

#ifndef __Game2D_OGL__GameMain__
#define __Game2D_OGL__GameMain__

#include <iostream>
#include "Shader.h"

/*
 
 This class will be our main game loop class
 
 All the other classes that will be required by the game will be created here.
 
 The viewcontroller class will only contain calls to this class's initialize, update and draw
 
 */

class GameMain

{
    
private:
    
    //our vertex buffer containing the geometry data for our quad
    unsigned int m_geometryBuffer;
    //buffer ID for the color data in the video memory
    unsigned int m_UVBuffer;
    
    //locations for the vertex in the shader
    int m_positionLocation;
    //location of the texture sampler and the uv attribute
    int m_textureLocation, m_uvLocation;
    
    //the shader program we use for rendering
    Shader *m_shader;
    
    //Texture Loading and Set-UP
    GLuint texture;
    GLuint setupTexture(NSString *filename);
    
public:
    
    GameMain();
    
    virtual ~GameMain();
    
    void Initialize();
    
    void Update(float elapsedTime);
    
    void Draw();
    
    void CleanUp();
    
};

#endif /* defined(__Game2D_OGL__GameMain__) */