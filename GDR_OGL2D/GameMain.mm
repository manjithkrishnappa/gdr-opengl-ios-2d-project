//
//  GameMain.cpp
//  GDR_OGL2D
//
//  Created by Manjith on 11/10/13.
//  Copyright (c) 2013 PGS. All rights reserved.
//

#include "GameMain.h"
#include <vector>
using namespace std;

GameMain::GameMain()

{
    
    fprintf(stderr, "\nGame Main Constructor");
    
}

GameMain::~GameMain()

{
    
    fprintf(stderr, "\nGame Main Destructor");
    
}

//Game Loop Initialization done here

void GameMain::Initialize()
{
    /************Loading the Texture and configuring its settings*****/
    //loading the texture
    //std::string fileName = "texture.png";
    texture = setupTexture(@"texture.png");
    
    //these settings are required to have non power of two textures loading
    // use linear filetring
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    // clamp to edge
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    /************Vertex Data Definition************/
    vector<float> geometryData;
    vector<float> uvData;
    
    //4 floats define one vertex (x, y, z and w), first one is lower left
    geometryData.push_back(-0.5f); geometryData.push_back(-0.5f); geometryData.push_back(0.0); geometryData.push_back(1.0);
    uvData.push_back(0.0f); uvData.push_back(1.0);
    
    //we go counter clockwise, so lower right vertex next
    geometryData.push_back(0.5f); geometryData.push_back(-0.5f); geometryData.push_back(0.0); geometryData.push_back(1.0);
    uvData.push_back(1.0f); uvData.push_back(1.0);
    
    //top left vertex is last
    geometryData.push_back(-0.5f); geometryData.push_back(0.5f); geometryData.push_back(0.0); geometryData.push_back(1.0);
    uvData.push_back(0.0f); uvData.push_back(0.0);
    
    //top right vertex is last
    geometryData.push_back(0.5f); geometryData.push_back(0.5f); geometryData.push_back(0.0); geometryData.push_back(1.0);
    uvData.push_back(1.0f); uvData.push_back(0.0);

    //generate an ID for our geometry buffer in the video memory and make it the active one
    glGenBuffers(1, &m_geometryBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_geometryBuffer);
    
    //send the data to the video memory
    glBufferData(GL_ARRAY_BUFFER, geometryData.size() * sizeof(float), &geometryData[0], GL_STATIC_DRAW);
    
    //generate an ID for the color buffer in the video memory and make it the active one
    glGenBuffers(1, &m_UVBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
    
    //send the data to the video memory
    glBufferData(GL_ARRAY_BUFFER, uvData.size() * sizeof(float), &uvData[0], GL_STATIC_DRAW);
    
    //load our shader
    m_shader = new Shader("shader.vert", "shader.frag");
    
    if(!m_shader->compileAndLink())
    {
        fprintf(stderr,"Encountered problems when loading shader, application will crash...");
    }
    
    //tell OpenGL to use this shader for all coming rendering
    glUseProgram(m_shader->getProgram());
    
    //get the attachment points for the attributes position and color
    m_positionLocation = glGetAttribLocation(m_shader->getProgram(), "position");
    m_uvLocation = glGetAttribLocation(m_shader->getProgram(), "texcoord");
    m_textureLocation = glGetUniformLocation(m_shader-> getProgram(), "mytexture");
    
    //check that the locations are valid, negative value means invalid
    if(m_positionLocation < 0 || m_uvLocation < 0 || m_textureLocation < 0)
    {
        fprintf(stderr,"Could not query attribute locations");
    }
    
}

GLuint GameMain::setupTexture(NSString *filename)
{
    CGImageRef spriteImage = [UIImage imageNamed:filename].CGImage;
    if (!spriteImage) {
        NSLog(@"Failed to load image %@", filename);
        exit(1);
    }
    size_t width = CGImageGetWidth(spriteImage);
    size_t height = CGImageGetHeight(spriteImage);
    GLubyte * spriteData = (GLubyte *) calloc(width*height*4, sizeof(GLubyte));
    CGContextRef spriteContext = CGBitmapContextCreate(spriteData, width, height, 8, width*4,
                                                       CGImageGetColorSpace(spriteImage), kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(spriteContext, CGRectMake(0, 0, width, height), spriteImage);
    CGContextRelease(spriteContext);
    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, spriteData);
    free(spriteData);
    return tex;
}

void GameMain::Update(float elapsedTime)

{
    
    //fprintf(stderr, "\nGame Main Update");
    
}

void GameMain::Draw()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(m_textureLocation, /*GL_TEXTURE*/0);
    
    glEnableVertexAttribArray(m_positionLocation);
    glEnableVertexAttribArray(m_uvLocation);
    
    
    //bind the geometry VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_geometryBuffer);
    //point the position attribute to this buffer, being tuples of 4 floats for each vertex
    glVertexAttribPointer(m_positionLocation, 4, GL_FLOAT, GL_FALSE, 0, NULL);
    
    //bind the uv VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
    //this attribute is only 3 floats per vertex
    glVertexAttribPointer(m_uvLocation, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    
    
    //initiate the drawing process
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(m_positionLocation);
    glDisableVertexAttribArray(m_uvLocation);
    
}

void GameMain::CleanUp()

{
    
}

