//
//  Shader.h
//  GDR_OGL2D
//
//  Created by Manjith on 14/10/13.
//  Copyright (c) 2013 PGS. All rights reserved.
//

#ifndef __GDR_OGL2D__Shader__
#define __GDR_OGL2D__Shader__

#include <iostream>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

//our shader class encapsulates all methods needed for loading and compiling shaders from a file
class Shader
{
public:
    
    //create a shader with these fragment and vertex shaders
    Shader(const std::string& vertexShaderFilename, const std::string& fragmentShaderFilename);
    
    //destructor just calls cleanup()
    ~Shader();
    
    //this methods invokes loading the files from disk, compiles the two shaders
    //and combines them to one shader program.
    //returns false if errors occurred
    bool compileAndLink();
    
    //returns the ID of the shader program which is needed to bind it etc
    unsigned int getProgram();


private:
    
    //cleans up the shaders, deletes them in the OpenGL context
    void cleanup();
    
    //loads a file and compiles it to either a GL_VERTEX_SHADER or GL_FRAGMENT_SHADER
    unsigned int loadAndCompileShaderFile(GLenum type, const std::string& filename);
    
    //links the program
    bool linkProgram();
    
    //---------------------------------------------------------
    //ids for the vertex and fragment shaders, and the final shader program
    unsigned int m_fragmentShader;
    unsigned int m_vertexShader;
    unsigned int m_shaderProgram;
    
    //store the filenames for the shaders until we actually load them
    std::string m_fragmentShaderFilename;
    std::string m_vertexShaderFilename;


};

#endif /* defined(__GDR_OGL2D__Shader__) */
