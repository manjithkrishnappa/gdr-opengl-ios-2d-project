//Simple pixel shader that simply outputs the texture

//Texture
uniform sampler2D mytexture;

//incoming values from the vertex shader stage.
//if the vertices of a primitive have different values, they are interpolated!
varying highp vec2 f_texcoord;

void main()
{
    gl_FragColor = texture2D(mytexture, f_texcoord);
}