//Simple vertex shader that takes the vertex position in screen co-ordinates and sets the vertex for drawing
// since the vertex is already in the screen co-ordinate we dont need to perform any transformation

//the incoming vertex' position
attribute vec4 position;
//the incoming vertex UV coords
attribute vec2 texcoord;

//UV coords to send to the fragment shader
varying highp vec2 f_texcoord;

//the shader entry point is the main method
void main()
{    
    f_texcoord = texcoord;
    gl_Position = position; //copy the position

}


