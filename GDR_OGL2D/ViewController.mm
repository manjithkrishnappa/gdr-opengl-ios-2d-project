//
//  ViewController.m
//  GDR_OGL2D
//
//  Created by Manjith on 11/10/13.
//  Copyright (c) 2013 PGS. All rights reserved.
//

//some irrelavant comit
//some other irrelavant comit 2
//Good luch at this comment

#import "ViewController.h"
#include "GameMain.h"

GameMain *_Main;

@interface ViewController ()
{
    
}

@property (strong, nonatomic) EAGLContext *context;

- (void)setupGL;

- (void)tearDownGL;

@end

@implementation ViewController

- (void)viewDidLoad

{
    
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        
        NSLog(@"Failed to create ES context");
        
    }
    
    GLKView *view = (GLKView *)self.view;
    
    view.context = self.context;
    
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    [self setupGL];
    
    _Main = new GameMain();
    _Main->Initialize();
    
}

- (void)dealloc

{
    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        
        
        [EAGLContext setCurrentContext:nil];
        
    }
    
    _Main->CleanUp();
    
}

- (void)didReceiveMemoryWarning

{
    
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            
            [EAGLContext setCurrentContext:nil];
            
        }
        
        self.context = nil;
        
    }
    
    // Dispose of any resources that can be recreated.
    
}

- (void)setupGL

{
    
    [EAGLContext setCurrentContext:self.context];
    
    glEnable(GL_CULL_FACE);
    
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glEnable(GL_BLEND);
    
}

- (void)tearDownGL

{
    
    [EAGLContext setCurrentContext:self.context];
    
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update

{
    _Main->Update(self.timeSinceLastUpdate);
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect

{
    
    glClearColor(0.65f, 0.65f, 0.65f, 1.0f);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    _Main->Draw();
}

@end